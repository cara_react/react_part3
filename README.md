# Partie III

Lien vers l’exercice : https://codepen.io/vlaude/pen/ZmEaXo?editors=0010

Dans cette troisième et dernière partie, nous allons faire un petit exercice d'**algorithmie** avec JavaScript et terminer l’application. Vous allez : 

- écrire l’algo qui va **mélanger** le Memory, i.e. créer des paires de cartes (on peut garder des valeurs allant de 1 à 8) et les positionner de manière aléatoire.

- injecter la variable isFind dans le component **Card**, qui défini si une carte est trouvée (i.e. la paire a été retourné) et traiter l’affichage de la carte en fonction (une carte trouvée est toujours retournée, et pourquoi pas changer son **style**).

**Résultat attendu :**

![](images/Resultat3.PNG)


**Hors périmètre :**

nous avons créé pour vous la fonction qui vérifie si une paire a été trouvé : checkCards. Vous n’avez pas à la modifier, vous pouvez la minimiser pour épurer l’affichage du code.

**Aide :** 

l’objectif pour la fonction **shuffleBoard**, est d’obtenir un tableau comme suit :
(par exemple)

[1, 3, 5, 1, 2, 5, 6, 6, 7, 3, 2, 4, 4, 8, 7, 8] 

Seulement, à la place de simples chiffres nous aurons des objets définis comme suit :

````
{
	index: number,
	value: number,
	isDisplay: boolean,
	isFind: boolean
}
````



**Bonus :**

Si vous avez terminé en avance et que vous êtes maintenant familier avec React et sa notion de component, pourquoi pas :

- Implémenter un score, défini par le nombre de clique que le joueur a fait avant de trouver toutes les paires.

- Et donc, implémenter une fonction qui vérifie si le joueur a gagné et qui affiche un message à l’écran.
